module.exports = {
    pluginOptions: {
      electronBuilder: {
        builderOptions: {
          extraResources: ['db.sqlite3'],
        },
        //This line: add knex and sqlite3
        externals: ['knex','sqlite3'],
      }
    }
  };