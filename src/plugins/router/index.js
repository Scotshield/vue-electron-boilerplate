import { createRouter, createWebHashHistory } from 'vue-router'
import Dashboard from './../../views/dashboard/Dashboard.vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/conta',
      name: 'conta',
      component: () => import('../../views/contas/List.vue')
    },
    {
      path: '/conta/new',
      name: 'conta.new',
      component: () => import('../../views/contas/Form.vue')
    },
    {
      path: '/conta/edit/:id',
      name: 'conta.edit',
      component: () => import('../../views/contas/Form.vue')
    },
    {
      path: '/sobre',
      name: 'sobre',
      component: () => import('../../views/sobre/Sobre.vue')
    },
    {
      path: '/cliente',
      name: 'cliente',
      component: () => import('../../views/clientes/List.vue')
    },
    {
      path: '/cliente/new',
      name: 'cliente.new',
      component: () => import('../../views/clientes/Form.vue')
    },
    {
      path: '/cliente/edit/:id',
      name: 'cliente.edit',
      component: () => import('../../views/clientes/Form.vue')
    },
    {
      path: '/fornecedor',
      name: 'fornecedor',
      component: () => import('../../views/fornecedores/List.vue')
    },
    {
      path: '/fornecedor/new',
      name: 'fornecedor.new',
      component: () => import('../../views/fornecedores/Form.vue')
    },
    {
      path: '/fornecedor/edit/:id',
      name: 'fornecedor.edit',
      component: () => import('../../views/fornecedores/Form.vue')
    },
    {
      path: '/atendentes',
      name: 'atendente',
      component: () => import('../../views/atendentes/List.vue')
    },
    {
      path: '/atendente/new',
      name: 'atendente.new',
      component: () => import('../../views/atendentes/Form.vue')
    },
    {
      path: '/atendente/edit/:id',
      name: 'atendente.edit',
      component: () => import('../../views/atendentes/Form.vue')
    },
    {
      path: '/marcas',
      name: 'marcas',
      component: () => import('../../views/marcas/List.vue')
    },
    {
      path: '/marca/new',
      name: 'marca.new',
      component: () => import('../../views/marcas/Form.vue')
    },
    {
      path: '/marca/edit/:id',
      name: 'marca.edit',
      component: () => import('../../views/marcas/Form.vue')
    },
    {
      path: '/categorias',
      name: 'categorias',
      component: () => import('../../views/categorias/List.vue')
    },
    {
      path: '/categoria/new',
      name: 'categoria.new',
      component: () => import('../../views/categorias/Form.vue')
    },
    {
      path: '/categoria/edit/:id',
      name: 'categoria.edit',
      component: () => import('../../views/categorias/Form.vue')
    },
    {
      path: '/formasPagamento',
      name: 'formasPagamento',
      component: () => import('../../views/formasPagamento/List.vue')
    },
    {
      path: '/formaPagamento/new',
      name: 'formaPagamento.new',
      component: () => import('../../views/formasPagamento/Form.vue')
    },
    {
      path: '/formaPagamento/edit/:id',
      name: 'formaPagamento.edit',
      component: () => import('../../views/formasPagamento/Form.vue')
    },
    {
      path: '/produtos',
      name: 'produtos',
      component: () => import('../../views/produtos/List.vue')
    },
    {
      path: '/produto/new',
      name: 'produto.new',
      component: () => import('../../views/produtos/Form.vue')
    },
    {
      path: '/produto/edit/:tipo/:id',
      name: 'produto.edit',
      component: () => import('../../views/produtos/Form.vue')
    }
  ]
})

export default router
