import { createApp } from 'vue'
import App from './App.vue'
import router from './plugins/router'
import "bootstrap/dist/js/bootstrap.js"
import "bootstrap/dist/css/bootstrap.min.css"
import "./assets/css/theme/sb-admin-2.min.css"
import '@fortawesome/fontawesome-free/js/all.js'
import '@fortawesome/fontawesome-free/css/all.css'
import Toaster from '@meforma/vue-toaster'
import money from "v-money3"
import axios from './plugins/axios'
import './assets/css/common.css'

const app = createApp(App)
    .use(router)
    .use(money)
    .use(Toaster)

app.config.globalProperties.axios=axios

app.mount('#app')
