const formasPagamentoModel = require('./../models/formasPagamento')

module.exports.listFormasPagamento = async (req, res) => {
    formasPagamentoModel.listFormasPagamento()
                .then(result => res.status(200).json({status: true, message: 'Formas de pagamento recuperadas com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar as formas de pagamentos cadastradas', data: err}))
}

module.exports.createFormaPagamento = async (req, res) => {
    const {nome} = req.body
    const formaPagamento = {nome, ativo: 1}

    formasPagamentoModel.createFormaPagamento(formaPagamento)
                .then(result => res.status(200).json({status:true, message: 'Forma de pagamento criada com sucesso', data: result}))
                .catch(err => {
                    console.log(err)
                    res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar a forma de pagamento', data: err})
                })
}

module.exports.findFormaPagamento = async (req, res) => {
    formasPagamentoModel.findFormaPagamento(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Forma de pagamento recuperada com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar a forma de pagamento informada', data: err}))
}

module.exports.updateFormaPagamento = async (req, res) => {
    const {id, nome} = req.body
    const formaPagamento = {id, nome, ativo: 1}

    formasPagamentoModel.updateFormaPagamento(formaPagamento)
                        .then(result => res.status(200).json({status:true, message: 'Forma de pagamento atualizada com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar a forma de pagamento', data: err}))
}

module.exports.deleteFormaPagamento = async (req, res) => {
    const deleteResponse = await formasPagamentoModel.deleteFormaPagamento(req.params.id)
    if(deleteResponse){
        const result = await formasPagamentoModel.listFormasPagamento()
        res.status(200).json({status: true, message: 'Forma de pagamento excluída com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir a forma de pagamento informada', data: []})
    }
}