const produtosModel = require('./../models/produtos')

module.exports.listProdutos = async (req, res) => {
    produtosModel.listProdutos()
                .then(result => res.status(200).json({status: true, message: 'Produtos recuperados com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar os produtos cadastrados', data: err}))
}

module.exports.createProduto = async (req, res) => {


    table.string('nome');
    table.string('codigo_barras');
    table.integer('marca_id').unsigned().index().references('id').inTable(tables.TABLE_MARCAS);
    table.integer('categoria_id').unsigned().index().references('id').inTable(tables.TABLE_CATEGORIAS);
    table.integer('estoque_minimo').defaultTo(1);
    table.integer('estoque_atual').defaultTo(1);
    table.integer('fornecedor_id').unsigned().index().references('id').inTable(tables.TABLE_FORNECEDORES);
    table.double('preco_compra');
    table.integer('margem_lucro').defaultTo(20);
    table.double('preco_venda');
    table.integer('comissao_referencia').defaultTo(0);
    table.string('observacao');
    table.string('foto');  



    const {nome, codigo_barras, marca_id, categoria_id, estoque_minimo, estoque_atual, fornecedor_id, preco_compra, margem_lucro, preco_venda, comissao_referencia, observacao, foto} = req.body
    const produto = {nome, codigo_barras, marca_id, categoria_id, estoque_minimo, estoque_atual, fornecedor_id, preco_compra, margem_lucro, preco_venda, comissao_referencia, observacao, foto, ativo: 1}

    produtosModel.createProduto(produto)
                .then(result => res.status(200).json({status:true, message: 'Produto criado com sucesso', data: result}))
                .catch(err => {
                    console.log(err)
                    res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar o produto', data: err})
                })
}

module.exports.findProduto = async (req, res) => {
    produtosModel.findProduto(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Produto recuperado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar o produto informado', data: err}))
}

module.exports.updateProduto = async (req, res) => {
    const {id, nome, codigo_barras, marca_id, categoria_id, estoque_minimo, estoque_atual, fornecedor_id, preco_compra, margem_lucro, preco_venda, comissao_referencia, observacao, foto} = req.body
    const produto = {id, nome, codigo_barras, marca_id, categoria_id, estoque_minimo, estoque_atual, fornecedor_id, preco_compra, margem_lucro, preco_venda, comissao_referencia, observacao, foto, ativo: 1}

    marcasModel.updateProduto(produto)
                        .then(result => res.status(200).json({status:true, message: 'Produto atualizado com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar o produto', data: err}))
}

module.exports.deleteProduto = async (req, res) => {
    const deleteResponse = await produtosModel.deleteProduto(req.params.id)
    if(deleteResponse){
        const result = await produtosModel.listProdutos()
        res.status(200).json({status: true, message: 'Produto excluído com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir o produto informado', data: []})
    }
}