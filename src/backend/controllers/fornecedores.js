const fornecedoresModel = require('./../models/fornecedores')

module.exports.listFornecedores = async (req, res) => {
    fornecedoresModel.listFornecedores()
                .then(result => res.status(200).json({status: true, message: 'Fornecedores recuperados com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar os fornecedores cadastrados', data: err}))
}

module.exports.createFornecedor = async (req, res) => {
    const {nome, email, telefone, cnpj, endereco} = req.body
    const fornecedor = {nome, email, telefone, cnpj, endereco, ativo: 1}

    fornecedoresModel.createFornecedores(fornecedor)
                .then(result => res.status(200).json({status:true, message: 'Fornecedor criado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar o fornecedor', data: err}))
}

module.exports.findFornecedor = async (req, res) => {
    fornecedoresModel.findFornecedores(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Fornecedor recuperado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar o fornecedor informado', data: err}))
}

module.exports.updateFornecedor = async (req, res) => {
    const {id, nome, email, telefone, cnpj, endereco} = req.body
    const fornecedor = {id, nome, email, telefone, cnpj, endereco, ativo: 1}

    fornecedoresModel.updateFornecedores(fornecedor)
                        .then(result => res.status(200).json({status:true, message: 'Fornecedor atualizado com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar o fornecedor', data: err}))
}

module.exports.deleteFornecedor = async (req, res) => {
    const deleteResponse = await fornecedoresModel.deleteFornecedores(req.params.id)
    if(deleteResponse){
        const result = await fornecedoresModel.listFornecedores()
        res.status(200).json({status: true, message: 'Fornecedor excluído com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir o fornecedor informado', data: []})
    }
}

