const marcasModel = require('./../models/marcas')

module.exports.listMarcas = async (req, res) => {
    marcasModel.listMarcas()
                .then(result => res.status(200).json({status: true, message: 'Marcas recuperadas com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar as marcas cadastradas', data: err}))
}

module.exports.createMarca = async (req, res) => {
    const {nome} = req.body
    const marca = {nome, ativo: 1}

    marcasModel.createMarca(marca)
                .then(result => res.status(200).json({status:true, message: 'Marca criada com sucesso', data: result}))
                .catch(err => {
                    console.log(err)
                    res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar a marca', data: err})
                })
}

module.exports.findMarca = async (req, res) => {
    marcasModel.findMarca(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Marca recuperada com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar a marca informada', data: err}))
}

module.exports.updateMarca = async (req, res) => {
    const {id, nome} = req.body
    const marca = {id, nome, ativo: 1}

    marcasModel.updateMarca(marca)
                        .then(result => res.status(200).json({status:true, message: 'Marca atualizada com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar a marca', data: err}))
}

module.exports.deleteMarca = async (req, res) => {
    const deleteResponse = await marcasModel.deleteMarca(req.params.id)
    if(deleteResponse){
        const result = await marcasModel.listMarcas()
        res.status(200).json({status: true, message: 'Marca excluída com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir a marca informada', data: []})
    }
}