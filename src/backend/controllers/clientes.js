const clientesModel = require('./../models/clientes')

module.exports.listClientes = async (req, res) => {
    clientesModel.listClientes()
                .then(result => res.status(200).json({status: true, message: 'Clientes recuperados com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar os clientes cadastrados', data: err}))
}

module.exports.createCliente = async (req, res) => {
    const {nome, telefone, endereco, email, data_nascimento, genero} = req.body
    const cliente = {nome, telefone, endereco, email, data_nascimento, genero, ativo: 1}

    clientesModel.createCliente(cliente)
                .then(result => res.status(200).json({status:true, message: 'Cliente criado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar o cliente', data: err}))
}

module.exports.findCliente = async (req, res) => {
    clientesModel.findCliente(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Cliente recuperado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar o cliente informado', data: err}))
}

module.exports.updateCliente = async (req, res) => {
    const {id, nome, telefone, endereco, email, data_nascimento, genero} = req.body
    const cliente = {id, nome, telefone, endereco, email, data_nascimento, genero, ativo: 1}

    clientesModel.updateCliente(cliente)
                        .then(result => res.status(200).json({status:true, message: 'Cliente atualizado com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar o cliente', data: err}))
}

module.exports.deleteCliente = async (req, res) => {
    const deleteResponse = await clientesModel.deleteCliente(req.params.id)
    if(deleteResponse){
        const result = await clientesModel.listClientes()
        res.status(200).json({status: true, message: 'Cliente excluída com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir o cliente informado', data: []})
    }
}

