const atendentesModel = require('./../models/atendentes')

module.exports.listAtendentes = async (req, res) => {
    atendentesModel.listAtendentes()
                .then(result => res.status(200).json({status: true, message: 'Atendentes recuperados com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar os atendentes cadastrados', data: err}))
}

module.exports.createAtendente = async (req, res) => {
    const {nome, cpf, email, telefone, foto, especialidade} = req.body
    const atendente = {nome, cpf, email, telefone, foto, especialidade, ativo: 1}

    atendentesModel.createAtendente(atendente)
                .then(result => res.status(200).json({status:true, message: 'Atendente criado com sucesso', data: result}))
                .catch(err => {
                    console.log(err)
                    res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar o atendente', data: err})
                })
}

module.exports.findAtendente = async (req, res) => {
    atendentesModel.findAtendente(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Atendente recuperado com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar o atendente informado', data: err}))
}

module.exports.updateAtendente = async (req, res) => {
    const {id, nome, cpf, email, telefone, foto, especialidade} = req.body
    const atendente = {id, nome, cpf, email, telefone, foto, especialidade, ativo: 1}

    atendentesModel.updateAtendente(atendente)
                        .then(result => res.status(200).json({status:true, message: 'Atendente atualizado com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar o atendente', data: err}))
}

module.exports.deleteAtendente = async (req, res) => {
    const deleteResponse = await atendentesModel.deleteAtendente(req.params.id)
    if(deleteResponse){
        const result = await atendentesModel.listAtendentes()
        res.status(200).json({status: true, message: 'Atendente excluído com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir o atendente informado', data: []})
    }
}

