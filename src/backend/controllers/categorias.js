const categoriasModel = require('./../models/categorias')

module.exports.listCategorias = async (req, res) => {
    categoriasModel.listCategorias()
                .then(result => res.status(200).json({status: true, message: 'Categorias recuperadas com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar as categorias cadastradas', data: err}))
}

module.exports.createCategoria = async (req, res) => {
    const {nome} = req.body
    const categoria = {nome, ativo: 1}

    categoriasModel.createCategoria(categoria)
                .then(result => res.status(200).json({status:true, message: 'Categoria criada com sucesso', data: result}))
                .catch(err => {
                    console.log(err)
                    res.status(500).json({status:false, message: 'Ocorreu um erro ao cadastrar a categoria', data: err})
                })
}

module.exports.findCategoria = async (req, res) => {
    categoriasModel.findCategoria(req.params.id)
                .then(result => res.status(200).json({status: true, message: 'Categoria recuperada com sucesso', data: result}))
                .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao recuperar a categoria informada', data: err}))
}

module.exports.updateCategoria = async (req, res) => {
    const {id, nome} = req.body
    const categoria = {id, nome, ativo: 1}

    categoriasModel.updateCategoria(categoria)
                        .then(result => res.status(200).json({status:true, message: 'Categoria atualizada com sucesso', data: result}))
                        .catch(err => res.status(500).json({status:false, message: 'Ocorreu um erro ao atualizar a categoria', data: err}))
}

module.exports.deleteCategoria = async (req, res) => {
    const deleteResponse = await categoriasModel.deleteCategoria(req.params.id)
    if(deleteResponse){
        const result = await categoriasModel.listCategorias()
        res.status(200).json({status: true, message: 'Categoria excluída com sucesso', data: result})
    }else{
        res.status(500).json({status:false, message: 'Ocorreu um erro ao excluir a categoria informada', data: []})
    }
}