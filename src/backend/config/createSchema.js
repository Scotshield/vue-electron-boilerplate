'use strict';

const db = require('./database')
const tables = require('./tableNames')

const up = () => {
    createPermissoes()
    createUsuarios()
    createClientes()
    createFornecedores()
    createAtendentes()
    createMarca()
    createCategoria()
    createFormasPagamento()
    createProdutos()
    createServicos()
    createAtendimentos()
    createAtendimentoProduto()
    createAtendimentoServico()
    createAtendimentoFormaPagamento()
}

const createPermissoes = async () => {
    return db.schema
        .hasTable(tables.TABLE_PERMISSOES)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_PERMISSOES, function (table) {
                    table.increments('id').primary();
                    table.string('nome').unique();
                    table.string('descricao');
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_PERMISSOES}`)
                );
            }
        });
};

const createUsuarios = async () => {
    return db.schema
        .hasTable(tables.TABLE_USUARIOS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_USUARIOS, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('email');
                    table.string('senha');
                    table.integer('ativo').defaultTo(1);
                    table.string('permissoes');
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_USUARIOS}`)
                );
            }
        });
};

const createClientes = async () => {
    return db.schema
        .hasTable(tables.TABLE_CLIENTES)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_CLIENTES, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('telefone');
                    table.integer('is_whatsapp').defaultTo(0); // 0 - Não é whatsapp, 1 - É whatsapp
                    table.string('endereco');
                    table.string('email');
                    table.date('data_nascimento');
                    table.string('genero');
                    table.string('foto');  // Caminho ou URL para a foto do cliente
                    table.integer('ativo').defaultTo(1)
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_CLIENTES}`)
                );
            }
        });
};

const createFornecedores = async () => {
    return db.schema
        .hasTable(tables.TABLE_FORNECEDORES)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_FORNECEDORES, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('email');
                    table.string('telefone');
                    table.integer('is_whatsapp').defaultTo(0); // 0 - Não é whatsapp, 1 - É whatsapp
                    table.string('cnpj');
                    table.string('endereco');
                    table.integer('ativo').defaultTo(1)
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_FORNECEDORES}`)
                );
            }
        });
};

const createAtendentes = async () => {
    return db.schema
        .hasTable(tables.TABLE_ATENDENTES)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_ATENDENTES, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('cpf');
                    table.string('email');
                    table.string('telefone');
                    table.integer('is_whatsapp').defaultTo(0); // 0 - Não é whatsapp, 1 - É whatsapp
                    table.string('foto');  // Caminho ou URL para a foto do atendente
                    table.string('especialidade');
                    table.integer('comissao_servico').defaultTo(10);  //ADICIONAR NA TELA
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_ATENDENTES}`)
                );
            }
        });
};

const createMarca = async () => {
    return db.schema
        .hasTable(tables.TABLE_MARCAS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_MARCAS, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_MARCAS}`)
                );
            }
        });
};

const createCategoria = async () => {
    return db.schema
        .hasTable(tables.TABLE_CATEGORIAS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_CATEGORIAS, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_CATEGORIAS}`)
                );
            }
        });
};

const createFormasPagamento = async () => {
    return db.schema
        .hasTable(tables.TABLE_FORMAS_PAGAMENTO)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_FORMAS_PAGAMENTO, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_FORMAS_PAGAMENTO}`)
                );
            }
        });
};

const createProdutos = async () => {
    return db.schema
        .hasTable(tables.TABLE_PRODUTOS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_PRODUTOS, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('codigo_barras');
                    table.integer('marca_id').unsigned().index().references('id').inTable(tables.TABLE_MARCAS);
                    table.integer('categoria_id').unsigned().index().references('id').inTable(tables.TABLE_CATEGORIAS);
                    table.integer('estoque_minimo').defaultTo(1);
                    table.integer('estoque_atual').defaultTo(1);
                    table.integer('fornecedor_id').unsigned().index().references('id').inTable(tables.TABLE_FORNECEDORES);
                    table.double('preco_compra');
                    table.integer('margem_lucro').defaultTo(20); //se margem de lucro for preenchida, o preço de venda será calculado a partir do preço de compra e da margem de lucro
                    table.double('preco_venda'); //se preço de venda for preenchido, a margem de lucro será calculada a partir do preço de compra e do preço de venda
                    table.integer('comissao_referencia').defaultTo(0); //serve como base para o cálculo da comissão do atendente, mas o usuário poderá alterar a comissão do atendente no campo comissao_efetiva da table atendimento_produto
                    table.string('observacao');
                    table.string('foto');  // Caminho ou URL para a foto do produto
                    table.integer('ativo').defaultTo(1);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_PRODUTOS}`)
                );
            }
        });
};

const createServicos = async () => {
    return db.schema
        .hasTable(tables.TABLE_SERVICOS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_SERVICOS, function (table) {
                    table.increments('id').primary();
                    table.string('nome');
                    table.string('observacao');
                    table.integer('duracao_estimada'); //em minutos
                    table.double('preco'); //preço padrão do serviço
                    table.string('foto');  // Caminho ou URL para a foto do serviço
                    table.integer('comissao_referencia').defaultTo(0); //serve como base para o cálculo da comissão do atendente, mas o usuário poderá alterar a comissão do atendente no campo comissao_efetiva da table atendimento_servico
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_SERVICOS}`)
                );
            }
        });
};

const createAtendimentos = async () => {
    return db.schema
        .hasTable(tables.TABLE_ATENDIMENTOS)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_ATENDIMENTOS, function (table) {
                    table.increments('id').primary();
                    table.integer('cliente_id').unsigned().index().references('id').inTable(tables.TABLE_CLIENTES);
                    table.dateTime('data_hora');
                    table.string('observacao');
                    table.integer('ativo').defaultTo(1);
                    table.double('valor_calculado'); //Este é um campo calculado, será calculado a partir dos produtos e serviços, o usuário não poderá alterar
                    table.double('desconto_calculado'); //Este é um campo calculado, será calculado a partir dos produtos e serviços, o usuário não poderá alterar
                    table.double('desconto_adicional_em_dinheiro').defaultTo(0); //Este é um campo que será preenchido pelo usuário e será subtraído do valor_calculado
                    table.double('valor_final'); //Este é um campo calculado, será calculado a partir dos produtos, serviços e descontos. O usuário não poderá alterar
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_ATENDIMENTOS}`)
                );
            }
        });
};

const createAtendimentoProduto = async () => {
    return db.schema
        .hasTable(tables.TABLE_ATENDIMENTO_PRODUTO)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_ATENDIMENTO_PRODUTO, function (table) {
                    table.increments('id').primary();
                    table.integer('atendimento_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDIMENTOS);
                    table.integer('produto_id').unsigned().index().references('id').inTable(tables.TABLE_PRODUTOS);
                    table.integer('quantidade');
                    table.double('desconto');
                    table.integer('comissao_efetiva').defaultTo(10);
                    table.integer('atendente_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDENTES);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_ATENDIMENTO_PRODUTO}`)
                );
            }
        });
};

const createAtendimentoServico = async () => {
    return db.schema
        .hasTable(tables.TABLE_ATENDIMENTO_SERVICO)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_ATENDIMENTO_SERVICO, function (table) {
                    table.increments('id').primary();
                    table.integer('atendimento_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDIMENTOS);
                    table.integer('servico_id').unsigned().index().references('id').inTable(tables.TABLE_SERVICOS);
                    table.double('desconto');
                    table.integer('comissao_efetiva').defaultTo(10);
                    table.integer('atendente_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDENTES);
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_ATENDIMENTO_SERVICO}`)
                );
            }
        });
};

const createAtendimentoFormaPagamento = async () => {
    return db.schema
        .hasTable(tables.TABLE_ATENDIMENTO_FORMA_PAGAMENTO)
        .then(function (exists) {
            if (!exists) {
                return db.schema.createTable(tables.TABLE_ATENDIMENTO_FORMA_PAGAMENTO, function (table) {
                    table.increments('id').primary();
                    table.integer('atendimento_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDIMENTOS);
                    table.integer('forma_pagamento_id').unsigned().index().references('id').inTable(tables.TABLE_FORMAS_PAGAMENTO);
                    table.double('valor');
                })
                .then(
                    console.log(`CREATED ${tables.TABLE_ATENDIMENTO_FORMA_PAGAMENTO}`)
                );
            }
        });
};


// const createContasPagar = async () => {
//     return db.schema
//         .hasTable(tables.TABLE_CONTAS_PAGAR)
//         .then(function (exists) {
//             if (!exists) {
//                 return db.schema.createTable(tables.TABLE_CONTAS_PAGAR, function (table) {
//                     table.increments('id').primary();
//                     table.integer('fornecedor_id').unsigned().index().references('id').inTable(tables.TABLE_FORNECEDORES);
//                     table.dateTime('data_vencimento');
//                     table.double('valor');
//                     table.boolean('pago').defaultTo(false); // Indica se a conta foi paga
//                 })
//                 .then(
//                     console.log(`CREATED ${tables.TABLE_CONTAS_PAGAR}`)
//                 );
//             }
//         });
// };

// const createContasReceber = async () => {
//     return db.schema
//         .hasTable(tables.TABLE_CONTAS_RECEBER)
//         .then(function (exists) {
//             if (!exists) {
//                 return db.schema.createTable(tables.TABLE_CONTAS_RECEBER, function (table) {
//                     table.increments('id').primary();
//                     table.integer('cliente_id').unsigned().index().references('id').inTable(tables.TABLE_CLIENTES);
//                     table.dateTime('data_vencimento');
//                     table.double('valor');
//                     table.boolean('recebido').defaultTo(false); // Indica se a conta foi recebida
//                 })
//                 .then(
//                     console.log(`CREATED ${tables.TABLE_CONTAS_RECEBER}`)
//                 );
//             }
//         });
// };


// const createMovimentoCaixa = async () => {
//     return db.schema
//         .hasTable(tables.TABLE_MOVIMENTO_CAIXA)
//         .then(function (exists) {
//             if (!exists) {
//                 return db.schema.createTable(tables.TABLE_MOVIMENTO_CAIXA, function (table) {
//                     table.increments('id').primary();
//                     table.dateTime('data_hora');
//                     table.string('descricao');
//                     table.double('valor');
//                     table.enum('tipo', ['entrada', 'saida']); // 'entrada' ou 'saida'
//                     table.integer('atendimento_id').unsigned().index().references('id').inTable(tables.TABLE_ATENDIMENTOS);
//                     table.integer('conta_pagar_id').unsigned().index().references('id').inTable(tables.TABLE_CONTAS_PAGAR);
//                     table.integer('conta_receber_id').unsigned().index().references('id').inTable(tables.TABLE_CONTAS_RECEBER);
//                 })
//                 .then(
//                     console.log(`CREATED ${tables.TABLE_MOVIMENTO_CAIXA}`)
//                 );
//             }
//         });
// };

module.exports = { up, createPermissoes, createUsuarios, createClientes, createFornecedores, createAtendentes, createMarca, createCategoria, createFormasPagamento, createProdutos, createServicos, createAtendimentos, createAtendimentoProduto, createAtendimentoServico, createAtendimentoFormaPagamento }