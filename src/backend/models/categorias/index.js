const db        = require('../../config/database')
const tables    = require('../../config/tableNames')

module.exports.listCategorias = async () => db(tables.TABLE_CATEGORIAS).select("*").where('ativo', 1)
module.exports.findCategoria = async (id) => db(tables.TABLE_CATEGORIAS).where('id', id).first("*")
module.exports.createCategoria = async (categoria) => db(tables.TABLE_CATEGORIAS).insert(categoria)
module.exports.updateCategoria = async (categoria) => db(tables.TABLE_CATEGORIAS).where('id', categoria.id).update(categoria)
module.exports.deleteCategoria = async (id) => {
    try{
        // const lancamentos = await db(tables.TABLE_LANCAMENTOS).where('id_cliente', id).select('*')
        // const lancamentos_repeticao = await db(tables.TABLE_LANCAMENTOS_REPETICAO).where('id_cliente', id).select('*')
        // const cartoes = await db(tables.TABLE_CARTOES).where('id_cliente', id).select('*')

        // if(lancamentos.length || lancamentos_repeticao.length || cartoes.length){
            await db(tables.TABLE_CATEGORIAS).where('id', id).update({ativo:0})
        // }else{
        // await db(tables.TABLE_CLIENTES).where('id', id).del()
        // }
        return true
    }catch(err){
        console.log(err)
        return false
    }
}