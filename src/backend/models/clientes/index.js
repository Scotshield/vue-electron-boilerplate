const db        = require('../../config/database')
const tables    = require('../../config/tableNames')

module.exports.listClientes = async () => db(tables.TABLE_CLIENTES).select("*").where('ativo', 1)
module.exports.findCliente = async (id) => db(tables.TABLE_CLIENTES).where('id', id).first('*')
module.exports.createCliente = async (cliente) => db(tables.TABLE_CLIENTES).insert(cliente)
module.exports.updateCliente = async (cliente) => db(tables.TABLE_CLIENTES).where('id', cliente.id).update(cliente)
module.exports.deleteCliente = async (id) => {
    try{
        // const lancamentos = await db(tables.TABLE_LANCAMENTOS).where('id_cliente', id).select('*')
        // const lancamentos_repeticao = await db(tables.TABLE_LANCAMENTOS_REPETICAO).where('id_cliente', id).select('*')
        // const cartoes = await db(tables.TABLE_CARTOES).where('id_cliente', id).select('*')

        // if(lancamentos.length || lancamentos_repeticao.length || cartoes.length){
            await db(tables.TABLE_CLIENTES).where('id', id).update({ativo:0})
        // }else{
        // await db(tables.TABLE_CLIENTES).where('id', id).del()
        // }
        return true
    }catch(err){
        console.log(err)
        return false
    }
}
