const db        = require('../../config/database')
const tables    = require('../../config/tableNames')

module.exports.listFormasPagamento = async () => db(tables.TABLE_FORMAS_PAGAMENTO).select("*").where('ativo', 1)
module.exports.findFormaPagamento = async (id) => db(tables.TABLE_FORMAS_PAGAMENTO).where('id', id).first("*")
module.exports.createFormaPagamento = async (formaPagamento) => db(tables.TABLE_FORMAS_PAGAMENTO).insert(formaPagamento)
module.exports.updateFormaPagamento = async (formaPagamento) => db(tables.TABLE_FORMAS_PAGAMENTO).where('id', formaPagamento.id).update(formaPagamento)
module.exports.deleteFormaPagamento = async (id) => {
    try{
        // const lancamentos = await db(tables.TABLE_LANCAMENTOS).where('id_cliente', id).select('*')
        // const lancamentos_repeticao = await db(tables.TABLE_LANCAMENTOS_REPETICAO).where('id_cliente', id).select('*')
        // const cartoes = await db(tables.TABLE_CARTOES).where('id_cliente', id).select('*')

        // if(lancamentos.length || lancamentos_repeticao.length || cartoes.length){
            await db(tables.TABLE_FORMAS_PAGAMENTO).where('id', id).update({ativo:0})
        // }else{
        // await db(tables.TABLE_CLIENTES).where('id', id).del()
        // }
        return true
    }catch(err){
        console.log(err)
        return false
    }
}