const db        = require('../../config/database')
const tables    = require('../../config/tableNames')

module.exports.listProdutos = async () => db(tables.TABLE_PRODUTOS).select("*").where('ativo', 1)
module.exports.findProduto = async (id) => db(tables.TABLE_PRODUTOS).where('id', id).first("*")
module.exports.createProduto = async (produto) => db(tables.TABLE_PRODUTOS).insert(produto)
module.exports.updateProduto = async (produto) => db(tables.TABLE_PRODUTOS).where('id', produto.id).update(produto)
module.exports.deleteProduto = async (id) => {
    try{
        // const lancamentos = await db(tables.TABLE_LANCAMENTOS).where('id_cliente', id).select('*')
        // const lancamentos_repeticao = await db(tables.TABLE_LANCAMENTOS_REPETICAO).where('id_cliente', id).select('*')
        // const cartoes = await db(tables.TABLE_CARTOES).where('id_cliente', id).select('*')

        // if(lancamentos.length || lancamentos_repeticao.length || cartoes.length){
            await db(tables.TABLE_PRODUTOS).where('id', id).update({ativo:0})
        // }else{
        // await db(tables.TABLE_CLIENTES).where('id', id).del()
        // }
        return true
    }catch(err){
        console.log(err)
        return false
    }
}