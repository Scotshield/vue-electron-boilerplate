const db        = require('../../config/database')
const tables    = require('../../config/tableNames')

module.exports.listMarcas = async () => db(tables.TABLE_MARCAS).select("*").where('ativo', 1)
module.exports.findMarca = async (id) => db(tables.TABLE_MARCAS).where('id', id).first("*")
module.exports.createMarca = async (marca) => db(tables.TABLE_MARCAS).insert(marca)
module.exports.updateMarca = async (marca) => db(tables.TABLE_MARCAS).where('id', marca.id).update(marca)
module.exports.deleteMarca = async (id) => {
    try{
        // const lancamentos = await db(tables.TABLE_LANCAMENTOS).where('id_cliente', id).select('*')
        // const lancamentos_repeticao = await db(tables.TABLE_LANCAMENTOS_REPETICAO).where('id_cliente', id).select('*')
        // const cartoes = await db(tables.TABLE_CARTOES).where('id_cliente', id).select('*')

        // if(lancamentos.length || lancamentos_repeticao.length || cartoes.length){
            await db(tables.TABLE_MARCAS).where('id', id).update({ativo:0})
        // }else{
        // await db(tables.TABLE_CLIENTES).where('id', id).del()
        // }
        return true
    }catch(err){
        console.log(err)
        return false
    }
}