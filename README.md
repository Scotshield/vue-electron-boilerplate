# Vue Electron Boilerplate

## Instalação
```
npm install
```

### Server
```
npm run electron:serve
```

### Build 
```
npm run electron:build
```

### Frameworks e bibliotecas principais
- axios
- bootstrap - 5.2.0
- express
- knex
- sqlite3
- v-money3
- vue-router
- vuex
